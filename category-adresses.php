<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
			<h1><?php single_cat_title(); ?></h1>
			<?php $category = get_category( get_query_var( 'cat' ) ); $cat_id = $category->cat_ID; ?>

			<form method="post" action="traitement.php">
			   <p>
			       <label for="pays">Quoi ?</label><br />
			       <select name="pays" id="pays">
			  	   <option value="france">France</option>
			           <option value="espagne">Espagne</option>
			           <option value="italie">Italie</option>
			           <option value="royaume-uni">Royaume-Uni</option>
			           <option value="canada">Canada</option>
			           <option value="etats-unis">États-Unis</option>
			           <option value="chine">Chine</option>
			           <option value="japon">Japon</option>
			       </select>
			   </p>
			</form>


			<?php set_query_var( 'top_post_category', $cat_id ); ?>
			<?php get_template_part('template-parts/general/top-article'); ?>

			<?php set_query_var( 'swiper_article', $cat_id ); ?>
			<?php get_template_part('template-parts/general/swiper-article'); ?>

			<?php $posts = get_posts(array('posts_per_page' => 12 )); ?>
			<?php foreach ( $posts as $post ) : setup_postdata( $post ); $current_post_id++;?>
					<?php $excludeIDS[] = $post->ID; ?>
					<?php
					if ($current_post_id%2 == 0){
						get_template_part('template-parts/general/article-horizontal-left'); } 
					else{
						get_template_part('template-parts/general/article-horizontal-right'); }
					?>
				
					<?php if ($current_post_id == 5){ ?><?php } ?>
					<?php if ($current_post_id == 10){ get_template_part('template-parts/general/most-popular'); } ?>


			<?php endforeach; wp_reset_postdata(); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>