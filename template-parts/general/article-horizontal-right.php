<?php 
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[1];
?>
<article class="article-odd">
	<div class="article-left-side">
		<div class="post-category-parent"><a href="<?php echo get_category_link($main_categorie->cat_ID) ?>"><?php echo $main_categorie->name; ?></a></div>
		<div class="post-category-child"><a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>"><?php echo $sous_categorie->name; ?></a></div>
		<div class="post-title"><?php the_title(); ?></div>
		<div class="post-share"></div>
	</div>
	<div class="article-right-side">
		<div class="post-thumbnail"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('small'); ?></a></div>
	</div>
</article>