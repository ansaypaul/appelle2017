<!-- swiper-video -->
<h3>SWIPER VIDEO</h3>
<?php $swiper_video = get_query_var('swiper_video') ?>
<h4>+ de <?php echo get_cat_name( $swiper_video[0] ) ?></h4>
	<div class="swiper-video">
	<?php $posts = get_posts(array('posts_per_page' => 5, 'category__and'=> $swiper_video , 'post__not_in' => $excludeIDS )); ?>
	<div class="swiper-container-video">
		<div class="swiper-wrapper">
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
		<?php $excludeIDS[] = $post->ID; ?>
			<div class="swiper-slide">
				<div class="swiper-image"><?php the_post_thumbnail('small'); ?></div>
				<div class="swiper-titre"><?php echo $post->post_title; ?></div>
			</div>
		<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<script>
    var swiperVideo = new Swiper('.swiper-container-video', {
        pagination: '.swiper-pagination',
        slidesPerView: 'auto',
        paginationClickable: true,
        spaceBetween: 30
    });
</script>
<!-- swiper-home -->