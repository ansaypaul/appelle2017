<!-- swiper-article -->
<h3>SWIPER ARTICLE</h3>
<?php $swiper_article = get_query_var('swiper_article');  echo $swiper_article; ?>
<h4>+ de <?php echo get_cat_name( $swiper_article ); ?></h4>
<div class="swiper-article swiper-article-<?php echo $swiper_article ?>">
	<?php $posts = get_posts(array('posts_per_page' => 5, 'category'=> $swiper_article , 'post__not_in' => $excludeIDS )); ?>
	<div class="swiper-container-article">
		<div class="swiper-wrapper">
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
		<?php $excludeIDS[] = $post->ID; ?>
			<div class="swiper-slide">
				<div class="swiper-image"><?php the_post_thumbnail('small'); ?></div>
				<div class="swiper-titre"><?php echo $post->post_title; ?></div>
			</div>
		<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<script>
    var swiperArticle = new Swiper('.swiper-container-article', {
        pagination: '.swiper-pagination',
        slidesPerView: 'auto',
        paginationClickable: true,
        spaceBetween: 30
    });
</script>
<!-- swiper-article -->