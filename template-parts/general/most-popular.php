<h3>LES PLUS LUS</h3>
<?php 
$time_most_popular = '1 days ago';
$posts_popular = get_posts(array('posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)))); 
foreach ( $posts_popular as $post ) : setup_postdata( $post ); ?>
<div class="most-popular-title"><a href="<?php echo the_permalink(); ?>"><?php the_title( '<h4>', '</h4>' ); ?></a></div>
<?php endforeach; wp_reset_postdata(); ?>