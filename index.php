<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
		<?php set_query_var( 'top_post_category', $category_home_promo ); ?>
		<?php get_template_part('template-parts/general/top-article'); ?>

		<?php set_query_var( 'swiper_article', $category_home_promo ); ?>
		<?php get_template_part('template-parts/general/swiper-article'); ?>

		<?php $posts = get_posts(array('posts_per_page' => 7, 'post__not_in' => $excludeIDS )); ?>

		<?php foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post_id++; ?>
			
			<?php get_template_part('template-parts/general/article'); ?>

			<?php if ($current_post_id == 1){ ?> <div class="imu"></div> <?php } ?>
			<?php if ($current_post_id == 3){ get_template_part('template-parts/general/most-popular'); } ?>

		<?php endforeach; wp_reset_postdata(); ?>
		
		<?php set_query_var( 'swiper_video', array($id_video_category) ); ?>
		<?php get_template_part('template-parts/general/swiper-video'); ?>

		<div class="instagram"></div> 
		<div class="imu"></div>
		<div class="social-network"></div>
		<div class="abonnement"></div>
		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>