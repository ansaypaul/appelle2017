read -p "Launch DB ! Press [ENTER] !"
export PATH=${PATH}:/Applications/MAMP/Library/bin
cd /Applications/MAMP/htdocs/ellev2017/wp-content/themes/ellev2017/app
mysql -u root -p << EOF
use mysql;
	DROP database IF EXISTS wordpress_init ;
 	CREATE DATABASE wordpress_init;
 	use wordpress_init;
 	source apptest.sql;
EOF
read -p "Chargement DB done ! Press [ENTER] !"