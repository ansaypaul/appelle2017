<!-- search -->
<form id="s_ajax" class="search" method="get" action="<?php echo home_url(); ?>" role="search">
<input autocomplete="off" id="input_ajax" x-webkit-speech="true" class="search-input" type="search" name="s" placeholder="Rechercher">
<button class="search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>


<div id="result_ajax" class="result_ajax">

</div>

<script>

/* <![CDATA[ */
var ajax_search = {"ajaxurl":"http://192.168.2.177:8080/ellev2017/wp-admin/admin-ajax.php"};
/* ]]> */
(function($) {
	$(document).on('keyup','#input_ajax',function( event ) {
		$number_of_char = $( "#input_ajax" ).val().length;
		if($number_of_char >= 3){
			$.ajax({
				url: ajax_search.ajaxurl,
				type: 'post',
				data: { action: 'ajax_pagination', s: $( "#input_ajax" ).val() },
				beforeSend: function() {
					$(document).scrollTop();
					$('#result_ajax').text('');
					$('#result_ajax').append( 'Loading New Posts...' );
				},
				success: function( html ) {
					if(html != ''){
						$('#result_ajax').text('');
						value = $( "#input_ajax" ).val();
						if($number_of_char >= 3){
							$('#result_ajax').append( html );
							$('#result_ajax .post-title').each(function( index ) {
								var replacedString = $( this ).text().replace(new RegExp(value, 'gi'), '<span style="color:red">' + value + '</span>');
								$( this ).html(replacedString);
							});
						}	
					}else{
						$('#result_ajax').text('');
						html = 'Aucun articles trouvés !';
						$('#result_ajax').append( html );
					}

				},
				error: function (xhr) {
					alert('error'); 
	   			} 

			})
		}else{
			$('#result_ajax').text('');
		}
	})
})(jQuery);

</script>

<!-- /search -->
