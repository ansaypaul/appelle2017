<?php



/** Check si la personne est un administrateur un editeur **/
function is_admin_onfrontend(){
	global $current_user; // Use global
	get_currentuserinfo(); // Make sure global is set, if not set it.
	if($current_user->roles[0] == 'administrator'){ return 1; }else{ return 0; }
}


/** single.php **/

/* compteur de page vues */
function elle_set_post_views(){
   		$count_key = 'wpb_post_views_count';
   		$postID = get_the_ID();
    	$count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '1');
	    	}
	    	else{
		    $count++;
		    if(!is_admin_onfrontend()){ 
		    	update_post_meta($postID, $count_key, $count);
		    }
	    }
}


/** category.php **/

/* + de articles de la même catégorie */
function get_plus_de_article($array_category,$number_article){
	$output = "";
	$posts = get_posts(array('posts_per_page' => $number_article,'category_in'=> array($id_category)));
	$output .= '<div class="swiper-container">';
	$output .= '<div class="swiper-wrapper">';
	$output .= '<div class="swiper-slide">Slide 1</div>';
	$output .= '<div class="swiper-slide">Slide 1</div>';
	foreach ( $posts as $post ) : setup_postdata( $post );
			$output .= '<div class="swiper-slide">Slide 2</div>';
	endforeach; wp_reset_postdata();
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<script>var swiper = new Swiper(".swiper-container");</script>';
	echo $output;
}